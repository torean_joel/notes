class Markdown {
  constructor({ data }) {
    this.data = {
      content: data.content || '',
      isReadonly: data.isReadonly || false
    };
    this.wrapper = undefined;
    // init settings
    this.settings = [
      {
        name: 'markdown-readonly',
        icon: `<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="far" data-icon="pen-to-square" class="svg-inline--fa fa-pen-to-square" role="img" viewBox="0 0 512 512"><path fill="currentColor" d="M495.6 49.23l-32.82-32.82C451.8 5.471 437.5 0 423.1 0c-14.33 0-28.66 5.469-39.6 16.41L167.5 232.5C159.1 240 154.8 249.5 152.4 259.8L128.3 367.2C126.5 376.1 133.4 384 141.1 384c.916 0 1.852-.0918 2.797-.2813c0 0 74.03-15.71 107.4-23.56c10.1-2.377 19.13-7.459 26.46-14.79l217-217C517.5 106.5 517.4 71.1 495.6 49.23zM461.7 94.4L244.7 311.4C243.6 312.5 242.5 313.1 241.2 313.4c-13.7 3.227-34.65 7.857-54.3 12.14l12.41-55.2C199.6 268.9 200.3 267.5 201.4 266.5l216.1-216.1C419.4 48.41 421.6 48 423.1 48s3.715 .4062 5.65 2.342l32.82 32.83C464.8 86.34 464.8 91.27 461.7 94.4zM424 288c-13.25 0-24 10.75-24 24v128c0 13.23-10.78 24-24 24h-304c-13.22 0-24-10.77-24-24v-304c0-13.23 10.78-24 24-24h144c13.25 0 24-10.75 24-24S229.3 64 216 64L71.1 63.99C32.31 63.99 0 96.29 0 135.1v304C0 479.7 32.31 512 71.1 512h303.1c39.69 0 71.1-32.3 71.1-72L448 312C448 298.8 437.3 288 424 288z"/></svg>`
      }
    ];
  }
  /**
   * static setup of the title and icon
   */
  static get toolbox() {
    return {
      title: 'Markdown',
      icon: '<svg xmlns="http://www.w3.org/2000/svg" width="208" height="128" viewBox="0 0 208 128"><path d="M30 98V30h20l20 25 20-25h20v68H90V59L70 84 50 59v39zm125 0l-30-33h20V30h20v35h20z"/></svg>'
    };
  }

  /**
   * The render settings that we use to update the view - toggle between edit and view mode
   */
  renderSettings() {
    const settingsWrapper = document.createElement('div');

    this.settings.forEach( markdownSettings => {
      let button = document.createElement('div');

      button.classList.add('cdx-settings-button');
      button.classList.add('custom-settings');
      button.innerHTML = markdownSettings.icon;

      //event handler on click
      button.addEventListener('click', () => {
        this._toggleEdit(markdownSettings.name);
        button.classList.toggle('cdx-settings-button--active');
      });

      settingsWrapper.appendChild(button);
    });

    return settingsWrapper;
  }

  onPaste(data) {
    console.log(data)
  }

 /**
  * The dom element that will be created to take the user data
  * @returns Dom element
  */
  render() {
    return this.renderDom();
  }

  /**
   * We take the content of the text area that we want to save
   * @param {ELM} blockContent 
   * @returns data we are saving of the text area
   */
  save(blockContent) {
    const editorData = blockContent.querySelector('.markdown-editor');
    return Object.assign(this.data, {
      content: editorData.innerText
    })
  }

  /**
   * This renders the data to the dom and based off the string we know what to render
   * @returns 
   */
  renderDom() {
    // check data so we know what to render
    this.wrapper = document.createElement('div');
    const markdownEditor = document.createElement("div");
    
    // add data - editable
    markdownEditor.contentEditable = true;

    // create attribute
    markdownEditor.classList.add('markdown-editor');

    //init data for the dom
    markdownEditor.innerHTML = this.data.content || '';
    
    this.wrapper.appendChild(markdownEditor);

    this._acceptEditState();
    return this.wrapper;
  }

  /**
   * Render Markdown - note we need to have the library markdown-it imported
   * (currently on root index as cdn)
   */
  renderMarkdown(data) { // the data we want to render
    console.log("data", this.data)
    const md = window.markdownit();
    return md.render(data);
  }

  /**
   * @private
   * Click on the Settings Button
   * @param {string} markdownSettings — tune name from this.settings
   */
  _toggleEdit(markdownSettings) {
    this.data[markdownSettings] = !this.data[markdownSettings];
    this._acceptEditState();
  }

  /**
   * Add specified class corresponds with activated edit states
   * @private
   */
  _acceptEditState() {
    this.settings.forEach(state => {
      console.log(this.wrapper)
      this.wrapper.classList.add('markdown-readonly')
      this.wrapper.classList.toggle(state.name, !!this.data[state.name]);
      // here we can remove content editable and change to markdown data
      const editorELm = document.querySelector('.markdown-editor');
      if (editorELm) {
        const isEditMode = document.querySelector('.markdown-readonly');
        // here we need to change the class
        if (isEditMode) {
          editorELm.classList.add('edit-mode');
          editorELm.setAttribute('contenteditable', true);
          editorELm.innerHTML = this.data.content
        } else {
          editorELm.classList.remove('edit-mode');
          editorELm.setAttribute('contenteditable', false);
          editorELm.innerHTML = this.renderMarkdown(editorELm.innerText)
        }
      }
    });
  }
}