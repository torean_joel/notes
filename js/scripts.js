// get the buttons
const saveBtn = document.querySelector('.icon-save');
const menubtn = document.querySelector('.icon-menu');
const newBtn = document.querySelector('.icon-add');
const sidebar = document.querySelector('.sidebar');
const wrapper = document.querySelector('.wrapper');

// add event listeners for the buttons
saveBtn.addEventListener('click', save);
menubtn.addEventListener('click', toggleMenu);
newBtn.addEventListener('click', addNew);

/**
 * Editor that inits the data and adds modules
 */
const editor = new EditorJS({
  holder: 'editor',
  autofocus: true,
  readOnly: false,
  /** 
   * Available Tools list. 
   * Pass Tool's class or Settings object for each Tool you want to use 
   */
  tools:{
    header:Header,
    delimiter: Delimiter,
    paragraph: {
      class: Paragraph,
      inlineToolbar: true,
    },
    embed: Embed,
    image: SimpleImage,
    code: {
      class: CodeTool,
      inlineToolbar: true,
    },
    marker: Marker,
    list: {
      class: List,
      inlineToolbar: true,
    },
    table: Table,
    checklist: Checklist,
    color: {
      class: ColorPlugin,
      config: {
         colorCollections: ['#FF1300','#EC7878','#9C27B0','#673AB7','#3F51B5','#0070FF','#03A9F4','#00BCD4','#4CAF50','#8BC34A','#CDDC39', '#FFF'],
         defaultColor: '#FF1300',
         type: 'text', 
      }     
    },
    strikethrough: Strikethrough,
    // markdown: Markdown,
  },
  onReady: () => {
    init();
  },
  data: loadNote() // based off time, it will load the last one initially
});

/**
 * init method
 */
function init() {
  updateMenu();
}

/**
 * Functions for the editor controls
 */

/**
 * save the file 
 * @param {*} editor 
 */
function save() {
  if (!editor) return;
  const title = document.querySelector('.title').value || 'untitled';
  // saving the data of the file
  editor.save().then((outputData) => {
    console.log("outputData", outputData)
    saveNote({id: randomId(), title, ...outputData})
  }).catch((error) => {
    console.log('Saving failed: ', error)
  });
}

/**
 * menuToggle the file 
 * @param {*} editor 
 */
function toggleMenu() {
  const sideBarDetails = getComputedStyle(sidebar)
  console.info("sideBarDetails", sideBarDetails.display)
  const isSidebarOpen = sideBarDetails.display || 'none';
  const headerElm = document.querySelector('.header');
  const editorElm = document.querySelector('#editor');

  if (isSidebarOpen === 'none') {
    menuToggleStyles('open');
    // here we check if the user clicks outside
    headerElm.addEventListener('click', menuToggleStyles);
    editorElm.addEventListener('click', menuToggleStyles);
    return;
  }
  headerElm.removeEventListener('click', menuToggleStyles);
  editorElm.removeEventListener('click', menuToggleStyles);
  menuToggleStyles();
  return;
}

/**
 * add the file 
 * @param {*} editor 
 */
function addNew() {
  if (!editor) return;
  // we need to load in and check if data was changed
  document.querySelector('.title').value = '';
  editor.clear();
}

/**
 * save to storgae
 */

function saveNote(data) {
  const notesData = JSON.parse(localStorage.getItem('notes')) || {};
  const initData = { ...notesData }
  // here we need to group and add versioning
  const addNewData = { ...initData, [data.id]: data };
  localStorage.setItem('notes', JSON.stringify(addNewData));
  updateMenu();
}

/**
 * Remove a note from memory
 * @param {number} id the id of the note that we want to remove
 */
function deleteNote(id) {
  const notesData = JSON.parse(localStorage.getItem('notes')) || {};
  if (!notesData[id]) return;
  const setNoteData = { ...notesData }
  delete setNoteData[id];
  localStorage.setItem('notes', JSON.stringify(setNoteData));
  updateMenu();
}

/**
 * Get the initial last item if there is data
 */
function loadNote(data) {
  const notesData = JSON.parse(localStorage.getItem('notes')) || {};
  var keys = Object.keys(notesData);
  var last = keys[keys.length - 1]
  if (!data && last) {
    document.querySelector('.title').value = notesData[last].title || '';
    return notesData[last] || {};
  }
  return notesData[data] || {}
}

/**
 * setup menu items and update
 */
function updateMenu() {
  const notesData = JSON.parse(localStorage.getItem('notes')) || {};
  if (!notesData) return;
  // update list of history
  const dataSet = [];
  sidebar.innerHTML = '';
  
  Object.keys(notesData).map((item) => {
    dataSet.unshift(notesData[item]);
  });
  // add to menu
  dataSet.forEach((note) => {
    var sideBarItem = document.createElement('div');
    var titleGroupElm = document.createElement('div');
    var timeElm = document.createElement('span');
    var deleteSidebarIcon = document.createElement('i');

    
    // create the data for the sidebar iem
    titleGroupElm.textContent = `${note.title}`;
    timeElm.textContent = `${dayjs(note.time).format("DD MMM YYYY - HH:mm:ss a")}`;
    
    titleGroupElm.setAttribute('class', `notes-wrapper`);
    timeElm.setAttribute('class', `notes-time`);
    sideBarItem.setAttribute('class', `notes-item`);
    sideBarItem.addEventListener('click', () => {
      editor.render(note);
      document.querySelector('.title').value = note.title || '';
    });
    // add delete icon dynamically
    deleteSidebarIcon.setAttribute('class', 'material-icons icon icon-delete');
    deleteSidebarIcon.textContent = `delete`;
    deleteSidebarIcon.addEventListener('click', (event) => {
      // we need to add a confirm here
      const deleteConfirm = window.confirm(`Are you sure you want to delete: ${note.title}`);
      if (!deleteConfirm) return;
      event.stopPropagation();
      deleteNote(note.id);
    });
    
    // set to sidebar
    titleGroupElm.appendChild(timeElm);
    sideBarItem.appendChild(titleGroupElm);
    sideBarItem.appendChild(deleteSidebarIcon);
    sidebar.appendChild(sideBarItem);
  });
}

/**
 * toggles the styles of the menu
 * @param {string} state the state of the menu 
 */
function menuToggleStyles(state) {
  console.log("state", state)
  switch (state) {
    case 'open':
      sidebar.style.display = "block";
      wrapper.style.gridTemplateColumns = "1fr 2fr";
      break;
      default:
      sidebar.style.display = "none";
      wrapper.style.gridTemplateColumns = "2fr";
      break;
  }
}

/**
 * Helpers
 */

/**
 * sort map
 * @param {object} myObj 
 */
 function sortObject(obj) {
  return Object.keys(obj).sort().reduce(function (result, key) {
      result[key] = obj[key];
      return result;
  }, {});
}

/**
 * Generates a random id that we can use
 * we need to look at something other than this
 * maybe snowflake or guid ids to have a lower chance of collision
 */
function randomId() {
  return Math.floor(Math.random() * 1E15);
}
